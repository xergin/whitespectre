module V1
  class GroupEventsController < ApplicationController
    expose(:group_event, attributes: :group_event_params)

    expose(:group_events) do |scope|
      scope.page(params[:page]).per(params[:per_page])
    end

    def index
      respond_with(group_events, serializer: PaginatedArraySerializer)
    end

    def show
      respond_with(group_event)
    end

    def create
      group_event.save
      respond_with(group_event)
    end

    def update
      group_event.save
      respond_with(group_event)
    end

    def destroy
      group_event.destroy
      respond_with(group_event)
    end

    private

    def group_event_params
      params.require(:group_event).permit(
        *%i(name description location status start_date end_date duration))
    end
  end
end
