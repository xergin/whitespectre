class AddDeletedAtToOpenEvents < ActiveRecord::Migration
  def change
    add_column :open_events, :deleted_at, :datetime
    add_index :open_events, :deleted_at
  end
end
