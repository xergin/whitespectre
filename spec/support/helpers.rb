module Helpers
  def json_response_body
    parse_json(response_body)
  end
end

RSpec.configure do |config|
  config.include Helpers
end
