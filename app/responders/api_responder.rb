class ApiResponder < ActionController::Responder
  def api_behavior
    if post?
      display(resource, status: :created)
    else
      display(resource, status: :ok)
    end
  end

  def json_resource_errors
    ApiError.new(status: :unprocessable_entity, validations: resource.errors)
  end
end
