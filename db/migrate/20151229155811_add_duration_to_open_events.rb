class AddDurationToOpenEvents < ActiveRecord::Migration
  def change
    add_column :open_events, :duration, :integer
  end
end
