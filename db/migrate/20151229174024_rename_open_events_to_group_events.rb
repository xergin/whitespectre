class RenameOpenEventsToGroupEvents < ActiveRecord::Migration
  def change
    rename_table :open_events, :group_events
  end
end
