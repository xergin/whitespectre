ActiveModel::Serializer.setup do |config|
  config.adapter = :json
  config.key_format = :lower
end
