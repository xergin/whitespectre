class GroupEventSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :location, :status, :start_date, :end_date, :duration,
             :deleted_at

  def filter(keys)
    keys.delete(:deleted_at) unless object.deleted?
    keys
  end
end
