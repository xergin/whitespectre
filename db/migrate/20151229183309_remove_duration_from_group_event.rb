class RemoveDurationFromGroupEvent < ActiveRecord::Migration
  def change
    remove_column :group_events, :duration
  end
end
