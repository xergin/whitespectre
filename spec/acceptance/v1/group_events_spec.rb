require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource 'Group Events' do
  header 'Accept', 'application/json'

  let!(:group_events) { create_list(:published_group_event, 10) }
  let(:group_event) { group_events.first }

  subject(:response) { json_response_body }

  get '/v1/group_events' do
    parameter :page, 'Page'
    parameter :per_page, 'Items per page'

    let(:params) { { page: 1, per_page: 25 } }

    example_request 'Get group events' do
      expect(response_status).to eq(200)
      expect(response['group_events'].first).to be_a_group_event_representation(group_events.first)
      expect(response['meta']).to be_a_meta_representation(group_events, params)
    end
  end

  get '/v1/group_events/:id' do
    let(:id) { group_event.id }

    example_request 'Get group event' do
      expect(response_status).to eq(200)
      expect(response['group_event']).to be_a_group_event_representation(group_event)
    end
  end

  post '/v1/group_events' do
    let(:params) do
      {
        group_event: attributes_for(:group_event)
      }
    end

    parameter :name, 'Name', required: true, scope: :group_event
    parameter :location, 'Location', scope: :group_event
    parameter :description, 'Description', scope: :group_event
    parameter :duration, 'Duration', scope: :group_event
    parameter :start_date, 'Start date', scope: :group_event
    parameter :end_date, 'End date', scope: :group_event

    example_request 'Create group event' do
      expect(response_status).to eq(201)
      expect(response['group_event']['status']).to eq('draft')
    end

    context 'with invalid params' do
      let(:params) do
        {
          group_event: {
            status: 'published',
            start_date: Date.current,
            end_date: Date.current
          }
        }
      end

      example_request 'Create invalid group event' do
        expect(response_status).to eq(422)
        expect(response['api_error']['validations']).to be
      end
    end

    context 'with published status' do
      let(:params) do
        {
          group_event: attributes_for(:published_group_event)
        }
      end

      parameter :name, 'Name', required: true, scope: :group_event
      parameter :location, 'Location', required: true, scope: :group_event
      parameter :description, 'Description', required: true, scope: :group_event
      parameter :duration, 'Duration', required: true, scope: :group_event
      parameter :start_date, 'Start date', required: true, scope: :group_event
      parameter :end_date, 'End date', required: true, scope: :group_event

      example_request 'Create published group event' do
        expect(response_status).to eq(201)
        expect(response['group_event']['status']).to eq('published')
      end
    end
  end

  put '/v1/group_events/:id' do
    let(:id) { group_event.id }
    let(:params) do
      { group_event: {name: 'New name'} }
    end

    example_request 'Update group event' do
      expect(response_status).to eq(200)
      expect(response['group_event']).to be_a_group_event_representation(group_event)
      expect(group_event.reload.name).to eq('New name')
    end
  end

  delete '/v1/group_events/:id' do
    let(:id) { group_event.id }

    example_request 'Delete group event' do
      expect(response_status).to eq(200)
      expect(response['group_event']).to be_a_deleted_group_event_representation(group_event)
    end
  end
end
