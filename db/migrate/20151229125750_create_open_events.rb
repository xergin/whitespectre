class CreateOpenEvents < ActiveRecord::Migration
  def change
    create_table :open_events do |t|
      t.string :name, null: false, default: ''
      t.string :location, null: false, default: ''

      t.text :description, null: false, default: ''

      t.integer :status, null: false, default: 0

      t.date :start_date
      t.date :end_date

      t.timestamps null: false
    end
  end
end
