FactoryGirl.define do
  factory :group_event do
    name
  end

  factory :published_group_event, parent: :group_event do
    status :published
    location 'test location'
    description 'test description'
    start_date { Date.current }
    end_date { Date.current + 30.days }
  end
end
