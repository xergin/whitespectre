class ApplicationController < ActionController::API
  include ActionController::Serialization
  include ActionController::ImplicitRender

  decent_configuration do
    strategy DecentExposure::StrongParametersStrategy
  end

  self.responder = ApiResponder

  respond_to :json
end
