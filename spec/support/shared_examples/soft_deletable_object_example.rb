shared_examples_for 'soft-deletable object' do
  before do
    subject.save(validate: false)
  end

  it 'prevents object deletion' do
    expect { subject.destroy }.not_to change { subject.class.with_deleted.count }
  end

  it 'sets deleted_at to object' do
    expect { subject.destroy }.to change { subject.deleted_at }
  end
end
