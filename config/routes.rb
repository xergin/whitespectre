Rails.application.routes.draw do
  namespace :v1, defaults: { format: 'json' } do
    resources :group_events, except: %i(new edit)
  end

  root to: 'v1/group_events#index', format: 'json'
end
