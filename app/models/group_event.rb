class GroupEvent < ActiveRecord::Base
  acts_as_paranoid

  enum status: %i(draft published)

  attr_accessor :duration

  validates :name, presence: true
  validates :location, :description, :start_date, :end_date, presence: true, if: :published?

  validates_date :start_date, on_or_after: :today, if: :published?
  validates_date :end_date, after: :start_date, if: :published?

  def duration=(value)
    self.end_date = start_date + value.to_i.days
  end

  def duration
    if end_date.present? && start_date.present?
      (end_date - start_date).to_i
    end
  end
end
