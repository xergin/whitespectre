RSpec::Matchers.define :be_a_deleted_group_event_representation do |group_event|
  match do |json|
    response_attributes = group_event.attributes.slice %w(
      id name description location status start_date end_date duration deleted_at
    )

    expect(json).to be
    expect(json).to include_attributes(response_attributes)
  end
end
