require 'rails_helper'

RSpec.describe GroupEvent, type: :model do
  it_behaves_like 'soft-deletable object'

  describe 'validations' do
    it { is_expected.to validate_presence_of :name }

    context 'when published' do
      before do
        subject.status = :published
      end

      it { is_expected.to validate_presence_of :location }
      it { is_expected.to validate_presence_of :description }
      it { is_expected.to validate_presence_of :start_date }
      it { is_expected.to validate_presence_of :end_date }

      describe 'start date' do
        it { is_expected.to allow_value(Date.current).for(:start_date) }
        it { is_expected.not_to allow_value(Date.yesterday).for(:start_date) }
      end

      describe 'end date' do
        before do
          subject.start_date = Date.current
        end

        it { is_expected.to allow_value(Date.current + 1.day).for(:end_date) }
        it { is_expected.not_to allow_value(Date.current).for(:end_date) }
      end
    end
  end
end
